#!/usr/bin/env python3

import flask
import os
from flask import send_from_directory

app = flask.Flask(__name__)
app.config["DEBUG"] = True

directory = './files'

@app.route('/xml-corr-pen', methods=["GET"])
def xml_corr_pen():
	response = send_from_directory(directory = directory, filename='corr-PEN.xml')
	return response

@app.route('/xml-init-pen', methods=["GET"])
def xml_init_pen():
	response = send_from_directory(directory = directory,filename='init-PEN.xml')
	return response

@app.route('/xml-corr-lip', methods=["GET"])
def xml_corr_lip():
	response = send_from_directory(directory = directory, filename='corr-LIP.xml')
	return response

@app.route('/xml-init-lip', methods=["GET"])
def xml_init_lip():
	response = send_from_directory(directory = directory, filename='init-LIP.xml')
	return response

@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

if __name__ == '__main__':
	app.run(host='0.0.0.0')
